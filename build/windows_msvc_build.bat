@echo off

if not exist %~dp0\..\bin ( mkdir %~dp0\..\bin )
pushd %~dp0\..\bin

:: ---------------------------------------
set APP_NAME=SampleApp
set APP_FILE=../src/application.cpp
set INCLUDES=-I "../src/"

set ENGINE_DIR=..\vendor\Candle
:: ---------------------------------------


:: ---------------------------------------
:: Set vcvars
:: ---------------------------------------
call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars64.bat" > nul
::call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin\amd64\vcvars64.bat"

:: ---------------------------------------
del *.exe > NUL 2> NUL
del *.pdb > NUL 2> NUL
del *.cod > NUL 2> NUL
del *.dll > NUL 2> NUL
del *.lib > NUL 2> NUL
del *.obj > NUL 2> NUL
del *.exp > NUL 2> NUL

:: ---------------------------------------
:: Common Options
:: ---------------------------------------
set baseOpts=-MT -nologo -DEBUG -Od -WX -W4 -FAsc -Zi
set ignoredWarn=-wd4311 -wd4302 -wd4201 -wd4505 -wd4100 -wd4189 -wd4273 -wd4005 -wd4244 -wd4305 -wd4996

:: ---------------------------------------
:: Engine Preprocessor
:: ---------------------------------------
echo [COMPILATION] Engine Preprocessor

if not exist "%ENGINE_DIR%/bin" ( mkdir "%ENGINE_DIR%/bin" )
pushd %ENGINE_DIR%\bin

cl %baseOpts% %ignoredWarn% -I "../src/" ..\src\meta\preprocessor.cpp /link

	pushd ..\src
		echo [PREPROCESSOR] Generating Engine Code MetaData
		..\bin\preprocessor.exe
	popd

popd

:: ---------------------------------------
:: Compiler Flags
:: ---------------------------------------
set includesPath=%INCLUDES% -I "%ENGINE_DIR%/src" -I "%ENGINE_DIR%/trdp/GLEW/include" -I "%ENGINE_DIR%/trdp/GLFW/include"
set commonCompilerFlags=-DCDL_DEBUG -DAPPLICATION_INCLUDE=%APP_FILE% -DGLEW_STATIC %baseOpts% %ignoredWarn% %includesPath%

:: ---------------------------------------
:: Application Library
:: ---------------------------------------
echo [COMPILATION] Application Library

set filenameFlags=/Fe"application.dll" /Fo"application.obj" /Fa"application.cod"
set applicationLibs=-LIBPATH:"%ENGINE_DIR%/trdp/GLEW/lib" glew32s.lib opengl32.lib user32.lib
set exportedFuncs=-EXPORT:EngineInit -EXPORT:EngineHotReload -EXPORT:EngineUpdateAndRender -EXPORT:EngineSetPlatform -EXPORT:EngineClean

cl %commonCompilerFlags% /LD %filenameFlags% %ENGINE_DIR%\src\engine\cdl_engine.cpp /link %applicationLibs% %exportedFuncs% -PDB:engine_%random%.pdb -incremental:no -opt:ref -ignore:4099

:: ---------------------------------------
:: Platform Executable
:: ---------------------------------------
echo [COMPILATION] Platform Executable

set platformLibsPath=-LIBPATH:"%ENGINE_DIR%/trdp/GLEW/lib" -LIBPATH:"%ENGINE_DIR%/trdp/GLFW/lib"
set platformLibs=glew32s.lib glfw3.lib gdi32.lib opengl32.lib user32.lib kernel32.lib msvcrt.lib comdlg32.lib shell32.lib Winmm.lib Xinput.lib /NODEFAULTLIB:LIBCMT
set linkerOptions=-incremental:no -opt:ref %platformLibsPath% %platformLibs%
set filenameFlags=/Fe"%APP_NAME%.exe" /Fo"%APP_NAME%.obj" /Fa"%APP_NAME%.cod"

cl %commonCompilerFlags% %filenameFlags% %ENGINE_DIR%\src\main.cpp /link %linkerOptions%

:: ---------------------------------------
:: Copy Engine Assets in relevant directory
:: ---------------------------------------
echo [MISC] Copying Engine Assets
if not exist "assets" ( mkdir "assets" )
if not exist "assets/engine" ( mkdir "assets/engine" )
Xcopy /E /I /y "%ENGINE_DIR%/assets/engine" "assets/engine" > nul

popd